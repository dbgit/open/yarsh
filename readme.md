# YARSH (Yet Another Rust SHell)

### Features

* Variables
* Command Aliases
* Simple Status Line
* Portable (works on windows, linux, macos and bsd)
* $PATH

### ToDo
* Pipes
* Tab Completion
* Syntax Highlighting
* Script Functionality
