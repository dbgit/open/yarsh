use std::env;
use std::path::Path;

pub fn exec(args: Vec<String>) {
    let home_dir = dirs::home_dir().unwrap();

    let new_dir = match args.get(0) {
        Some(d) => d.to_owned(),
        None => home_dir.to_str().unwrap().to_owned(),
    };

    let root = Path::new(&new_dir);
    if let Err(e) = env::set_current_dir(&root) {
        eprintln!("{}", e);
    }
}
