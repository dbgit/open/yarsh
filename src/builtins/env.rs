use colored::*;
use std::collections::HashMap;

pub fn exec(variables: &HashMap<String, String>) {
    for (key, val) in variables {
        println!("{} {}", key.green(), val.blue());
    }
}
