use colored::*;
use std::cmp::Ordering;
use std::fs::*;

pub fn exec(args: Vec<String>) {
    let paths = read_dir(".").unwrap();

    let prepared_paths: Vec<DirEntry> = paths.filter(|p| p.is_ok()).map(|p| p.unwrap()).collect();

    let mut folders: Vec<DirEntry> = vec![];
    let mut files: Vec<DirEntry> = vec![];

    for entry in prepared_paths {
        let t = get_file_type(&entry);
        if t == ListFileType::Folder {
            folders.push(entry);
        } else {
            files.push(entry);
        }
    }

    let get_name = |x: &DirEntry| {
        let name = x.file_name();
        let file_name_str = name.to_str().unwrap().to_owned();
        file_name_str.to_lowercase()
    };

    let sort_vecs = |x: &mut Vec<DirEntry>| {
        x.sort_by(|a, b| {
            let a_name = get_name(&a);
            let b_name = get_name(&b);
            a_name.cmp(&b_name)
        })
    };

    sort_vecs(&mut folders);
    sort_vecs(&mut files);

    folders.extend(files);

    let show_hidden = args.contains(&"-a".to_owned());

    for path in folders.into_iter() {
        let file_name = path.file_name();
        let file_name = file_name.to_str().unwrap();
        let file_type = get_file_type(&path);

        if !show_hidden && file_name.starts_with('.') {
            continue;
        }

        let file_icon = match path.path().extension() {
            None => get_file_or_folder_icon(&file_type),
            Some(os_str) => match os_str.to_str() {
                Some("lock") => "🔒",
                Some("md") => "🧾",
                Some("rs") => "⚙",
                Some("js") => "⚙",
                Some("json") => "⚙",
                _ => get_file_or_folder_icon(&file_type),
            },
        };

        match &file_type {
            x if *x == ListFileType::Folder => println!("{} {}", file_icon, file_name.blue()),
            x if *x == ListFileType::File => println!("{} {}", file_icon, file_name.green()),
            _ => println!("{} {}", file_icon, file_name.red()),
        }
    }
}

fn get_file_type(t: &DirEntry) -> ListFileType {
    if t.file_type().unwrap().is_dir() {
        ListFileType::Folder
    } else {
        ListFileType::File
    }
}

fn get_file_or_folder_icon(file_type: &ListFileType) -> &str {
    match file_type {
        x if *x == ListFileType::Folder => "📂",
        _ => "🗄",
    }
}

#[derive(PartialEq, Eq)]
enum ListFileType {
    Folder = 0,
    File = 1,
}

impl PartialOrd for ListFileType {
    fn partial_cmp(&self, _other: &Self) -> Option<Ordering> {
        match self {
            x if *x == ListFileType::File => Some(Ordering::Greater),
            x if *x == ListFileType::Folder => Some(Ordering::Less),
            _ => Some(Ordering::Less),
        }
    }
}

impl Ord for ListFileType {
    fn cmp(&self, _other: &Self) -> Ordering {
        match self {
            x if *x == ListFileType::File => Ordering::Greater,
            x if *x == ListFileType::Folder => Ordering::Less,
            _ => Ordering::Less,
        }
    }
}
