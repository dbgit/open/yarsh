use colored::*;
use std::collections::HashMap;

pub fn exec(args: Vec<String>, variables: &mut HashMap<String, String>) {
    let key = match args.get(0) {
        Some(k) => k.to_owned(),
        None => {
            println!("{}", "Failed to set variable".red());
            return;
        }
    };
    let val = match args.get(1) {
        Some(k) => (*k).to_owned(),
        None => {
            println!("{}", "Failed to set variable".red());
            return;
        }
    };
    variables.insert(key, val);
}
