mod builtins;
mod std_ext;
mod utils;

use std::collections::HashSet;
use std::process::Command;

use colored::*;
use rustyline::error::ReadlineError;
use rustyline::validate::{ValidationContext, ValidationResult, Validator};
use rustyline::{Config, Editor};
use rustyline_derive::{Completer, Helper, Highlighter, Hinter};
use stopwatch::Stopwatch;

use utils::*;

#[derive(Completer, Helper, Highlighter, Hinter)]
struct InputValidator {
    command_list: HashSet<String>,
}

impl Validator for InputValidator {
    fn validate(&self, ctx: &mut ValidationContext) -> Result<ValidationResult, ReadlineError> {
        let input: Vec<_> = ctx.input().split_whitespace().collect();
        let command = match input.get(0) {
            Some(c) => *c,
            None => return Ok(ValidationResult::Valid(None)),
        };
        match self.command_list.get(command) {
            Some(_) => Ok(ValidationResult::Valid(None)),
            None => Ok(ValidationResult::Invalid(Some(format!(
                "\n{} {}",
                "Command not found:".bold().red(),
                command
            )))),
        }
    }
}

fn main() {
    let mut variables = environment_loader::load_variables();
    let mut aliases = environment_loader::load_aliases();
    let path = variables.get("PATH").unwrap().to_owned();
    let commands = environment_loader::discover_executables(path, &aliases);
    let mut stopwatch = Stopwatch::new();

    let config = Config::builder()
        .auto_add_history(true)
        .history_ignore_space(true)
        .build();
    let h = InputValidator {
        command_list: commands.keys().cloned().collect(),
    };
    let mut rl = Editor::with_config(config);
    rl.set_helper(Some(h));

    loop {
        status_line::print(stopwatch.elapsed());
        let readline = rl.readline("$ ");
        match readline {
            Ok(line) => {
                rl.add_history_entry(line.as_str());
                if let Some(inputs) = input_parser::parse(line, &mut variables, &mut aliases) {
                    command_executor::exec(inputs, &mut stopwatch, &mut variables, &mut aliases)
                }
            }
            Err(ReadlineError::Interrupted) => {
                // do nothing...
            }
            Err(err) => {
                println!("Error: {:?}", err);
                break;
            }
        }
    }
}
