use crate::std_ext::traits::DrainEnd;

impl DrainEnd for String {
    fn drain_end(&mut self, n: usize) -> &mut Self {
        let self_len = self.len();
        if self_len < n {
            self.truncate(0);
        } else {
            self.truncate(self_len - n);
        }
        self
    }
}

#[cfg(test)]
mod string_ext_tests {
    use super::*;

    #[test]
    fn drain_from_end() {
        let mut test_string = "Hello World".to_owned();
        let expected = "Hello".to_owned();
        assert_eq!(test_string.drain_end(6).as_str(), expected);
    }

    #[test]
    fn drain_from_end_but_input_longer_than_string() {
        let mut test_string = "Hello World".to_owned();
        let expected = "".to_owned();
        assert_eq!(test_string.drain_end(60).as_str(), expected);
    }
}
