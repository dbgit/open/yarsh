pub trait DrainEnd {
    fn drain_end(&mut self, n: usize) -> &mut Self;
}
