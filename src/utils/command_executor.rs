use crate::utils::input_parser::Input;
use crate::*;
use std::collections::HashMap;
use std::process::{Child, Stdio};
use stopwatch::Stopwatch;

pub fn exec(
    inputs: Vec<Input>,
    stopwatch: &mut Stopwatch,
    variables: &mut HashMap<String, String>,
    aliases: &mut HashMap<String, String>,
) {
    let mut commands = inputs.into_iter().peekable();
    let mut previous_command = None;

    stopwatch.start();
    while let Some(next_command) = commands.next() {
        let command = &next_command.command;
        let args = next_command.args.clone();

        match command.as_ref() {
            "exit" => builtins::exit::exec(),
            "set" => builtins::set::exec(args, variables),
            "alias" => builtins::set::exec(args, aliases),
            "env" => builtins::env::exec(variables),
            "builtin:cd" => builtins::cd::exec(args),
            "builtin:cat" => builtins::cat::exec(args),
            "builtin:ls" => builtins::ls::exec(args),
            "builtin:echo" => builtins::echo::exec(args),
            "builtin:pwd" => builtins::pwd::exec(),
            command => {
                let stdin = previous_command.map_or(Stdio::inherit(), |output: Child| {
                    Stdio::from(output.stdout.unwrap())
                });
                let stdout = if commands.peek().is_some() {
                    Stdio::piped()
                } else {
                    Stdio::inherit()
                };
                let output = Command::new(command)
                    .args(args)
                    // Re-enable variables
                    // .envs(variables)
                    .stdin(stdin)
                    .stdout(stdout)
                    .spawn();
                match output {
                    Ok(child) => {
                        previous_command = Some(child);
                    }
                    Err(_) => {
                        previous_command = None;
                        println!("{} {}", "Command not found:".bold().red(), command)
                    }
                };
            }
        };
    }

    if let Some(mut final_command) = previous_command {
        final_command
            .wait()
            .expect("Failed to wait for command to finish");
    }

    stopwatch.stop();
}
