use crate::std_ext::traits::DrainEnd;
use std::collections::{HashMap, HashSet};
use std::env;
use std::error::Error;
use std::fs;

pub fn load_variables() -> HashMap<String, String> {
    let mut variables: HashMap<String, String> = HashMap::new();
    env::vars().for_each(|(key, val)| {
        variables.insert(key, val);
    });

    if !variables.contains_key("PATH") {
        variables.insert("PATH".to_owned(), "/bin:/usr/bin:/usr/local/bin".to_owned());
    }

    variables
}

pub fn load_aliases() -> HashMap<String, String> {
    let mut aliases: HashMap<String, String> = HashMap::new();
    aliases.insert("cd".to_owned(), "builtin:cd".to_owned());
    aliases.insert("cat".to_owned(), "builtin:cat".to_owned());
    aliases.insert("ls".to_owned(), "builtin:ls".to_owned());
    aliases.insert("echo".to_owned(), "builtin:echo".to_owned());
    aliases.insert("pwd".to_owned(), "builtin:pwd".to_owned());

    aliases
}

pub fn discover_executables(
    path: String,
    aliases: &HashMap<String, String>,
) -> HashMap<String, CommandLocation> {
    let mut commands: HashMap<String, CommandLocation> = HashMap::new();

    let mut paths: HashSet<_> = path.split(':').collect();
    paths.insert(".");

    for p in paths {
        // Fix for windows, apparently paths are written as "\\folder\childFolder;C" (except for the last one in the vec)
        let sanitized_path = p.replace(";C", "");
        match fs::read_dir(sanitized_path) {
            Err(e) => println!("Failed to read path: \"{}\" because {}", p, e.description()),
            Ok(folder_content) => {
                for item in folder_content {
                    if let Ok(entry) = item {
                        let file = entry.metadata().unwrap().file_type();
                        if !file.is_dir() {
                            let mut name = entry.file_name().to_str().unwrap().to_owned();
                            let file_path = entry.path().to_str().unwrap().to_owned();

                            if (name.ends_with(".exe") || name.ends_with(".bat")) && name.len() > 4
                            {
                                name.drain_end(4);
                            }

                            commands.insert(name, CommandLocation::Executable(file_path));
                        }
                    }
                }
            }
        }
    }

    for (alias, target) in aliases {
        commands.insert(alias.to_owned(), CommandLocation::Alias(target.to_owned()));
    }

    commands.insert(
        "cd".to_owned(),
        CommandLocation::Alias("builtin:cd".to_owned()),
    );
    commands.insert(
        "cat".to_owned(),
        CommandLocation::Alias("builtin:cat".to_owned()),
    );
    commands.insert(
        "ls".to_owned(),
        CommandLocation::Alias("builtin:ls".to_owned()),
    );
    commands.insert(
        "echo".to_owned(),
        CommandLocation::Alias("builtin:echo".to_owned()),
    );
    commands.insert(
        "pwd".to_owned(),
        CommandLocation::Alias("builtin:pwd".to_owned()),
    );
    commands.insert("exit".to_owned(), CommandLocation::Builtin);
    commands.insert("env".to_owned(), CommandLocation::Builtin);
    commands.insert("set".to_owned(), CommandLocation::Builtin);
    commands.insert("alias".to_owned(), CommandLocation::Builtin);

    commands
}

pub enum CommandLocation {
    Builtin,
    Alias(String),
    Executable(String),
}
