use lazy_static::*;
use regex::{Match, Regex};
use std::collections::HashMap;

lazy_static! {
    static ref ARG_REGEX: Regex = Regex::new(r#"(?P<pipe>\|)?(?P<arg>"[^"]+"|[^\s"]+)?"#).unwrap();
}

#[derive(Debug, PartialEq, Eq)]
pub struct Input {
    pub command: String,
    pub args: Vec<String>,
}

pub fn parse(
    input: String,
    variables: &mut HashMap<String, String>,
    aliases: &mut HashMap<String, String>,
) -> Option<Vec<Input>> {
    let mut commands: Vec<Input> = vec![];
    let input_parts = split_by_pipe(&input);

    for part in input_parts {
        if let Some(parsed) = parse_input_single(&mut part.to_owned(), variables, aliases) {
            commands.push(parsed);
        }
    }

    Some(commands)
}

// ToDo: use this function
// Err: this function's return type contains a borrowed value with an elided lifetime, but the lifetime cannot be derived from the arguments
fn split_by_pipe(input: &String) -> Vec<Vec<Match>> {
    let mut input_parts: Vec<Vec<Match>> = vec![];
    let mut command_num = 0;
    let mut command_current: Vec<Match> = vec![];

    ARG_REGEX.captures_iter(&input)
        .for_each(|r| {
            if r.name("pipe").is_some() {
                input_parts.insert(command_num, command_current.clone().to_owned());
                command_num = command_num + 1;
                command_current = vec![];
            } else {
                command_current.push(r.name("arg").unwrap());
            }
        });
    input_parts.insert(command_num, command_current);

    input_parts
}

fn parse_input_single(
    parts: &mut Vec<Match>,
    variables: &mut HashMap<String, String>,
    aliases: &mut HashMap<String, String>,
) -> Option<Input> {
    let mut parts: Vec<_> = parts
        .iter_mut()
        .map(|r| r.as_str())
        .map(|r| replace_vars(r, variables))
        .collect();

    let command = match parts.get(0) {
        Some(t) => {
            let cmd = t;
            match aliases.get(cmd) {
                Some(c) => c.to_owned(),
                None => cmd.to_owned(),
            }
        }
        None => return None,
    };

    if command.is_empty() {
        return None;
    }

    let args = parts.split_off(1);

    Some(Input { command, args })
}

fn replace_vars(r: &str, variables: &mut HashMap<String, String>) -> String {
    if r.starts_with('$') {
        match variables.get(&r.replace('$', "")) {
            Some(var) => var.to_owned(),
            None => "".to_owned(),
        }
    } else {
        r.to_owned()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_test() {
        let input = "git ls-tree -r --name-only HEAD | rg --files | ag -l -g | find .".to_owned();
        let mut variables: HashMap<String, String> = HashMap::new();
        let mut aliases: HashMap<String, String> = HashMap::new();

        let mut expected: Vec<Input> = vec![];
        let mut args0: Vec<String> = vec![];
        args0.push("ls-tree".to_owned());
        args0.push("-r".to_owned());
        args0.push("--name-only".to_owned());
        args0.push("HEAD".to_owned());
        expected.push(Input {
            command: "git".to_owned(),
            args: args0,
        });

        let mut args1: Vec<String> = vec![];
        args1.push("--files".to_owned());
        expected.push(Input {
            command: "rg".to_owned(),
            args: args1,
        });

        let mut args2: Vec<String> = vec![];
        args2.push("-l".to_owned());
        args2.push("-g".to_owned());
        expected.push(Input {
            command: "ag".to_owned(),
            args: args2,
        });

        let mut args3: Vec<String> = vec![];
        args3.push(".".to_owned());
        expected.push(Input {
            command: "find".to_owned(),
            args: args3,
        });

        let result = parse(input, &mut variables, &mut aliases).unwrap();

        assert_eq!(result, expected);
    }

    #[test]
    fn replace_vars_test() {
        let r = "$abc";
        let mut variables: HashMap<String, String> = HashMap::new();
        variables.insert("abc".to_owned(), "foobar".to_owned());

        let result = replace_vars(&r, &mut variables);

        assert_eq!(result, "foobar");
    }
}
