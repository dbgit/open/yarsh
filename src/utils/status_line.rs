use colored::*;
use std::env;
use std::io::{stdout, Write};
use std::time::Duration;

pub fn print(previous_command_duration: Duration) {
    let current_dir = match env::current_dir() {
        Ok(dir) => {
            let dir = match dir.file_name() {
                Some(t) => t.to_str().unwrap(),
                None => dir.to_str().unwrap(),
            };
            dir.to_owned()
        }
        Err(_) => "???".to_owned(),
    };

    let took = previous_command_duration.as_secs();

    println!();
    if took > 1 {
        let took_str = format!("took {}s", took);
        println!("{} {}", current_dir.cyan(), took_str.yellow());
    } else {
        println!("{}", current_dir.cyan());
    }

    stdout().flush().expect("failed to write to STDOUT");
}
